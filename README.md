# HiOrg-Server als Keycloak Identity Provider

[English version](README.en.md)

Dies ist eine Erweiterung für [Keycloak](https://www.keycloak.org/), welche es
erlaubt, den Dienst [HiOrg-Server](https://www.hiorg-server.de/) als Identity
Provider zu nutzen.

[![Build Status](https://gitlab.com/MartinBoehmer/keycloak-hiorgserver-idp/badges/master/pipeline.svg)](https://gitlab.com/MartinBoehmer/keycloak-hiorgserver-idp/pipelines)

## Features

Die Erweiterung nutzt die [OAuth2-Schnittstelle des HiOrg-Server](https://wiki.hiorg-server.de/admin/oauth2).
Sie funktioniert analog zu den "Social Logins" über Facebook, Google, Twitter, usw.
und stellt für Keycloak die Funktion "Mit HiOrg-Server anmelden" bereit.
Auf diese Weise kann die **Anmeldung an Keycloak über HiOrg-Server** erfolgen und
ein **in Keycloak geführtes Benutzerkonto mit deinem HiOrg-Server-Konto verknüpft**
werden. Es ist auch möglich, sich initial mit dem HiOrg-Server-Konto bei Keycloak
zu registrieren, falls dies in der Keycloak-Konfiguration aktiviert wird.

Eine Besonderheit der Erweiterung ist die Möglichkeit, **HiOrg-Server-Konten in
verschiedenen Organisationen verknüpfen**
 zu können.
Beispiel: Organisation A und Organisation B haben beide einen HiOrg-Server mit
getrennten Organisationskürzeln (org-a, org-b). Wenn ein Benutzer HiOrg-Server-
Konten in beiden Organisationen hat, können diese beide mit seinem in Keycloak
geführten Benutzerkonto verknüpft werden, so dass er sich über beide
Organisationen an Keycloak anmelden kann.

## Einrichtung

### Voraussetzungen

Die Erweiterung erfordert eine lauffähige Keycloak-Instanz. Die Kompatibilität ist wie folgt:

:white_check_mark: Keycloak 9.0.x wird unterstützt 

:x: Alle anderen Keycloak-Versionen werden nicht unterstützt, wenngleich die Erweiterung damit funktionieren könnte

Wenn du die Kompatibilität der Erweiterung ausweiten möchtest, schaue bitte unter [Mitwirken](#mitwirken).

### Deployment

Die Erweiterung besteht aus nur einer Datei: `keycloak-hiorgserver-idp-X.X.X.jar`.

Diese muss entweder [selbst kompiliert (siehe unten)](#kompilieren) oder als [einsatzbereites
Release](https://gitlab.com/MartinBoehmer/keycloak-hiorgserver-idp/releases)
 heruntergeladen werden.
 
Angenommen, Keycloak befindet sich im Verzeichnis `/opt/keycloak`. Die JAR-Datei
muss, abhängig vom Betriebsmodus, in den folgenden Unterordner kopiert werden:
 
 * `/opt/keycloak/standalone/deployments` (Standalone-Modus)
 
Fertig. Keycloak lädt und aktiviert die Erweiterung nun automatisch.

Zur Kontrolle sollte das Keycloak-Log überprüft werden.

### Theme anpassen

Die Erweiterung enthält ein Theme, welches beispielhaft die notwendigen Anpassung
zeigt. Das Theme muss manuell über die Admin-Konsole für ein Realm aktiviert
werden.

Das **Login-Theme** besteht aus dem HiOrg-Server-Logo und wenigen Erweiterungen in
Form von CSS. Beides ist im [resource-Ordner des Login-Themes](https://gitlab.com/MartinBoehmer/keycloak-hiorgserver-idp/-/tree/master/src%2Fmain%2Fresources%2Ftheme%2Fhiorgsrv-theme%2Flogin%2Fresources) 
zu finden und lässt sich auf einfache Weise auch in ein anderes Theme übernehmen.

Hinweis: Urheberin und Eigentümerin des [HiOrg-Server-Logos](https://gitlab.com/MartinBoehmer/keycloak-hiorgserver-idp/-/blob/master/src/main/resources/theme/hiorgsrv-theme/login/resources/img/hiorg_logo_50px.png)
ist die [HiOrg Server GmbH](https://info.hiorg-server.de/impressum/),
mit dessen freundlicher Genehmigung das Logo hier bereitgestellt wird.

### Identity Provider anlegen

HiOrg-Server kann nun über die Keycloak-Admin-Console als Identity Provider (IDP) hinzugefügt werden. Dabei ist folgendes zu beachten:

* Das Organisationskürzel kann nur beim Anlegen des IDP eingestellt werden. Es lässt sich später nicht mehr verändern, da es Teil des eindeutigen Alias und damit der Endpunkt-URL wird.
* Um die Verküpfung eines Keycloak-Benutzerkontos mit mehreren Benutzerkonten aus verschiedenen HiOrg-Server-Organisationen zu ermöglichen, muss je Organisationskürzel ein HiOrg-Server IDP hinzugefügt werden.
* Wird kein Organisationskürzel festgelegt, so lässt sich ein Keycloak-Benutzerkonto folglich nur mit einem einzigen HiOrg-Server-Benutzerkonto verknüfen
* Die Benutzerdaten (Attribute), die vom HiOrg-Server bereitgestelt werden, sind in einem [Beispiel-Datensatz in der HiOrg-Server-Dokumentation](https://wiki.hiorg-server.de/_media/admin/user.json) ersichtlich.

Screenshots:

<img src="media/add-hiorg-server-idp.png" width="300">
<img src="media/add-hiorg-server-idp-details.png" width="300">

### Authenticator einsetzen

Die Erweiterung liefert einen Authenticator mit, um den Umgang mit den den HiOrg-Server-Benutzern genauer zu steuern. Funktionen:

* Sicherstellen, dass der HiOrg-Server-Benutzer tatsächlich aus der Organisation kommt, die für den IDP, über den er sich authentifiziert hat, eingestellt wurde

Um den Authenticator zu nutzen, sind folgende Schritte erforderlich:

* Eine Kopie des "First Broker Login" Authentication Flows anlegen, hier "Copy Of Frist Broker Login"
* Dem neuen Flow mittels "Add execution" den "HiOrg-Server Authenticator" hinzufügen
* Den ergänzten Authenticator an den Anfang der Flow verschieben, so dass er als erste Aktion nach dem Login über HiOrg-Server ausgeführt wird
* Unter "Requirement" für den Authenticator "REQUIRED" wählen
* Bei Bedarf unter "Actions" eine Konfiguration für den Authenticator anlegen. Dies ist aufgrund der Standardwerte der Einstellungen jedoch optional.
* In den Einstellungen der HiOrg-Server IDPs den neuen Flow als "First Login Flow" auswählen

Screenshots:

<img src="media/add-hiorg-server-authn.png" width="300">
<img src="media/hiorg-server-authn-flow.png" width="300">
<img src="media/hiorg-server-authn-config.png" width="300">


## Kompilieren

Die Erweiterung wird wie folgt kompiliert:

```
mvn clean install
```

Im Unterverzeichnis `target` befindet sich nun die Datei `keycloak-hiorgserver-idp-X.X.X.jar`.

In einer Entwicklungsumgebung kann sie alternativ mittels des folgenden Befehls deployt werden:

```
mvn clean install wildfly:deploy
```


## Mitwirken

Die Mitwirkung an diesem Projekt ist willkommen! Es gibt dafür verschiedene Möglichkeiten:

* Melde einen Fehler oder frag nach einer Verbesserung, indem du einen [Issue erstellst](https://gitlab.com/MartinBoehmer/keycloak-hiorgserver-idp/-/issues)
* Trage Code, Dokumentation oder andere Inhalte bei, indem du einen [Pull/Merge-Request erstellen](https://gitlab.com/MartinBoehmer/keycloak-hiorgserver-idp/-/merge_requests)

Ein Beitrag ist an die folgenden Bedingungen gebunden:

* Durch deinen Beitrag stimmst du zu, dass dieser unter der [für das Projekt festgelegten Lizenz](LICENSE) lizensiert wird
* Du musst die erforderlichen Rechte für deinen Beitrag besitzen (z. B. Modifikations- oder Urheberrechte)
