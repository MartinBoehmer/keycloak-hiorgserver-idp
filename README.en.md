# HiOrg-Server as a Keycloak Identity Provider

[Deutsche Version](README.md)

This is an extension for [Keycloak](https://www.keycloak.org/), which allows
to use the service [HiOrg-Server](https://www.hiorg-server.de/) as an Identity
Provider.

[![Build Status](https://gitlab.com/MartinBoehmer/keycloak-hiorgserver-idp/badges/master/pipeline.svg)](https://gitlab.com/MartinBoehmer/keycloak-hiorgserver-idp/pipelines)

## Features

The extension uses the [OAuth2 API of HiOrg-Server](https://wiki.hiorg-server.de/admin/oauth2).
It works similar to the "Social Logins" via Facebook, Google, Twitter, etc.
and provides "Login with HiOrg-Server" functionality for Keycloak.
This way, users can **login to Keycloak via HiOrg-Server** and 
**link a Keycloak account with a HiOrg-Server account**.
In addition, it is possible to register a new account in Keycloak with an existing HiOrg-Server account,
in case Keycloak is configured to support this scenario.

A special feature of this extension is the ability to **link a single account in Keycloak with multiple
HiOrg-Server account from different organisations**.
Example: Organisation A and Organisation B both use HiOrg-Server with their respective 
Organisation Codes (org-a, org-b). In case a users has HiOrg-Server accounts for/in both
organisations, they can both be linked to his (single) Keycloak account. As a result, the
user is able to login to Keycloak using his HiOrg-Server accounts from both organisations.

## Setup

### Requirements

This extension requires a running Keycloak instance. Compatibility is as follows:

:white_check_mark: Keycloak 9.0.x is supported

:x: Any other Keycloak version is not supported, although it might work

If you want to extend the compatibility of this extension, check out the [Contribute section](#contribute).

### Deployment

The extension is provided as a single archive: `keycloak-hiorgserver-idp-X.X.X.jar`.

You may either [compile it yourself (see below)](#build) or download a [ready-to-use 
release](https://gitlab.com/MartinBoehmer/keycloak-hiorgserver-idp/releases).
 
Let's assume Keycloak is located in the directory `/opt/keycloak`. The JAR file
must copied to one of the following location (depending on Keycloak's operating mode):
 
 * `/opt/keycloak/standalone/deployments` (Standalone Mode)
 
Done. Keycloak will load and activate the extension automatically.

Verify that everything worked out by checking Keycloak's server log.

### Theme Customisation

The extension contains a theme that shows examples for the necessary adjustments.
The theme must be activated manually for a realm via the admin console.

The included **Login Theme** conists of the HiOrg-Server logo and a few additions to the CSS.
Both can be found in the [resource directory of the Login Theme](https://gitlab.com/MartinBoehmer/keycloak-hiorgserver-idp/-/tree/master/src%2Fmain%2Fresources%2Ftheme%2Fhiorgsrv-theme%2Flogin%2Fresources) 
and easily applied to another theme.

Note: Copyright and ownership of the [HiOrg-Server logo](https://gitlab.com/MartinBoehmer/keycloak-hiorgserver-idp/-/blob/master/src/main/resources/theme/hiorgsrv-theme/login/resources/img/hiorg_logo_50px.png)
is held by [HiOrg Server GmbH](https://info.hiorg-server.de/impressum/),
with whose kind permission the logo is provided here.

### Add Identity Provider

HiOrg-Server can be added as an Identity Provider (IDP) via the Keycloak Admin Console. Please note the following:

* The Organisation Code can only be set when initially creating the IDP. It cannot be changed
afterwards, because it becomes a part of the unique alias and hence of the endpoint URL.
* To be able to link a Keycloak account with multiple accounts from different HiOrg-Server
organisations, you need to add a HiOrg-Server IDP for each Organisation Code.
* In case no Organisation Code is set, a Keycloak account may only be linked with a single HiOrg-Server account
* The user attributes provided by HiOrg-Server are documented in an [example record in the official HiOrg-Server
documentation](https://wiki.hiorg-server.de/_media/admin/user.json).

Screenshots:

<img src="media/add-hiorg-server-idp.png" width="300">
<img src="media/add-hiorg-server-idp-details.png" width="300">

### Authenticator Usage

The extension includes an Authenticator to control the handling of HiOrg-Server users in more detail.
Features:

* Enforce that a HiOrg-Server user is from the organisation, that was configured for the IDP he/she
authenticated with

To setup the Authenticator, perform the following steps:

* Copy the "First Broker Login" authentication flows and name it e. g. "Copy Of Frist Broker Login"
* Add an execution to the copied flow by selecting "HiOrg-Server Authenticator" as the provider
* Move the added Authenticator to the top/start of the flow, so that is the first action performed after logging in with HiOrg-Server
* Under "Requirement" set "REQUIRED" for the Authenticator 
* You may create a configuration for the Authenticator by clicking "Actions". This step is optional due to the configuration defaults.
* Set the new flow to be the "First Login Flow" in the settings of the HiOrg-Server IDPs 

Screenshots:

<img src="media/add-hiorg-server-authn.png" width="300">
<img src="media/hiorg-server-authn-flow.png" width="300">
<img src="media/hiorg-server-authn-config.png" width="300">


## Build

The extension can be compiled by the following command:

```
mvn clean install
```

Afterwards, there is a sub-directory called `target` with the archive file `keycloak-hiorgserver-idp-X.X.X.jar`.

In a development setup you may build and deploy the extension using the alternative command:

```
mvn clean install wildfly:deploy
```


## Contribute

Contributions are welcome! You can contribute in different ways:

* Report a bug or ask for an improvement by [creating an Issue](https://gitlab.com/MartinBoehmer/keycloak-hiorgserver-idp/-/issues)
* Contribute code, documentation or other content by [creating a Pull/Merge Request](https://gitlab.com/MartinBoehmer/keycloak-hiorgserver-idp/-/merge_requests)

Contributions are subject to the following conditions:

* By contributing you agree to license your contribution under the [project's license](LICENSE)
* You need to have the legal rights to make the contribution (e. g. modification rights or copyright)
