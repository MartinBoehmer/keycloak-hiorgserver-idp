/**
 * Copyright 2020 Martin Böhmer
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keycloak.broker.hiorgserver;

import com.fasterxml.jackson.databind.JsonNode;
import java.io.IOException;
import org.keycloak.broker.oidc.AbstractOAuth2IdentityProvider;
import org.keycloak.broker.oidc.mappers.AbstractJsonUserAttributeMapper;
import org.keycloak.broker.provider.BrokeredIdentityContext;
import org.keycloak.broker.provider.IdentityBrokerException;
import org.keycloak.broker.provider.util.SimpleHttp;
import org.keycloak.events.EventBuilder;
import org.keycloak.models.KeycloakSession;

/**
 * Keycloak identity provider for
 * <a href="https://www.hiorg-server.de/">HiOrg-Server</a>.
 *
 * @author Martin Böhmer
 */
public class HiOrgServerIdentityProvider
        extends AbstractOAuth2IdentityProvider<HiOrgServerIdentityProviderConfig> {

    public static final String AUTH_URL = "https://www.hiorg-server.de/api/oauth2/v1/authorize.php";
    public static final String TOKEN_URL = "https://www.hiorg-server.de/api/oauth2/v1/token.php";
    public static final String PROFILE_URL = "https://www.hiorg-server.de/api/oauth2/v1/user.php";

    public static final String DEFAULT_SCOPE = "basic eigenedaten";

    public HiOrgServerIdentityProvider(KeycloakSession session, HiOrgServerIdentityProviderConfig config) {
        super(session, config);
        config.setAuthorizationUrl(AUTH_URL);
        config.setTokenUrl(TOKEN_URL);
        config.setUserInfoUrl(PROFILE_URL);
    }

    @Override
    protected boolean supportsExternalExchange() {
        return true;
    }

    @Override
    protected String getProfileEndpointForValidation(EventBuilder event) {
        return getConfig().getUserInfoUrl();
    }

    @Override
    protected BrokeredIdentityContext extractIdentityFromProfile(EventBuilder event, JsonNode node) {
        HiOrgServerIdentityContextWrapper wrapper = new HiOrgServerIdentityContextWrapper(getJsonProperty(node, "user_id"));
        BrokeredIdentityContext identity = wrapper.getContext();
        identity.setUsername(getJsonProperty(node, "username_at_orga"));
        identity.setFirstName(getJsonProperty(node, "vorname"));
        identity.setLastName(getJsonProperty(node, "name"));
        identity.setEmail(getJsonProperty(node, "email"));
        wrapper.setOrganisationCode(getJsonProperty(node, "orga"));
        identity.setIdpConfig(getConfig());
        identity.setIdp(this);

        AbstractJsonUserAttributeMapper.storeUserProfileForMapper(identity, node, getConfig().getAlias());
        return identity;
    }

    @Override
    protected BrokeredIdentityContext doGetFederatedIdentity(String accessToken) {
        try {
            JsonNode profile = SimpleHttp.doGet(getConfig().getUserInfoUrl(),
                    session).header("Authorization", "Bearer " + accessToken).asJson();
            return extractIdentityFromProfile(null, profile);
        } catch (IOException e) {
            throw new IdentityBrokerException("Could not obtain user profile from HiOrg-Server.", e);
        }
    }

    @Override
    protected String getDefaultScopes() {
        return DEFAULT_SCOPE;
    }

}
