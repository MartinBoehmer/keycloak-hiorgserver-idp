/**
 * Copyright 2020 Martin Böhmer
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keycloak.broker.hiorgserver;

import org.keycloak.broker.provider.BrokeredIdentityContext;

/**
 * Facade of {@link BrokeredIdentityContext} for
 * {@link HiOrgServerIdentityProvider}.
 *
 * @author Martin Böhmer
 */
public class HiOrgServerIdentityContextWrapper {

    private static final String ORGCODE_ATTRIBUTE_NAME = "hiorgsrv_ov";

    private final BrokeredIdentityContext context;

    public HiOrgServerIdentityContextWrapper(String id) {
        this.context = new BrokeredIdentityContext(id);
    }

    public HiOrgServerIdentityContextWrapper(BrokeredIdentityContext context) {
        this.context = context;
    }

    public BrokeredIdentityContext getContext() {
        return context;
    }

    public String getOrganisationCode() {
        return context.getUserAttribute(ORGCODE_ATTRIBUTE_NAME);
    }

    public void setOrganisationCode(String code) {
        context.setUserAttribute(ORGCODE_ATTRIBUTE_NAME, code);
    }

}
