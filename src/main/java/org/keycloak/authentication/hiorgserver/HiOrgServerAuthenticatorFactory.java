/**
 * Copyright 2020 Martin Böhmer
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keycloak.authentication.hiorgserver;

import java.util.Arrays;
import java.util.List;
import org.keycloak.Config;
import org.keycloak.authentication.Authenticator;
import org.keycloak.authentication.AuthenticatorFactory;
import org.keycloak.models.AuthenticationExecutionModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;
import org.keycloak.provider.ProviderConfigProperty;

/**
 * Factory for {@link HiOrgServerAuthenticator}.
 *
 * @author Martin Böhmer
 */
public class HiOrgServerAuthenticatorFactory implements AuthenticatorFactory {

    public static final String PROVIDER_ID = "hiorgsrv-authenticator";

    public static final String ORGCODE_RESTRICTION_CONFIG = "hiorgsrv_orgcode_restriction";

    private static final HiOrgServerAuthenticator SINGLETON = new HiOrgServerAuthenticator();

    private static final AuthenticationExecutionModel.Requirement[] HIORG_REQUIREMENT_CHOICES = {
        AuthenticationExecutionModel.Requirement.REQUIRED,
        AuthenticationExecutionModel.Requirement.DISABLED
    };

    @Override
    public Authenticator create(KeycloakSession session) {
        return SINGLETON;
    }

    @Override
    public void init(Config.Scope config) {
        // NOOP
    }

    @Override
    public void postInit(KeycloakSessionFactory factory) {
        // NOOP
    }

    @Override
    public void close() {
        // NOOP
    }

    @Override
    public String getId() {
        return PROVIDER_ID;
    }

    @Override
    public String getDisplayType() {
        return "HiOrg-Server Authenticator";
    }

    @Override
    public String getReferenceCategory() {
        return getDisplayType();
    }

    @Override
    public String getHelpText() {
        return "Control access based on user data provided by HiOrg-Server";
    }

    @Override
    public boolean isConfigurable() {
        return true;
    }

    @Override
    public AuthenticationExecutionModel.Requirement[] getRequirementChoices() {
        return HIORG_REQUIREMENT_CHOICES;
    }

    @Override
    public boolean isUserSetupAllowed() {
        return false;
    }

    @Override
    public List<ProviderConfigProperty> getConfigProperties() {
        ProviderConfigProperty orgCodeRestriction = new ProviderConfigProperty();
        orgCodeRestriction.setType(ProviderConfigProperty.BOOLEAN_TYPE);
        orgCodeRestriction.setDefaultValue("true");
        orgCodeRestriction.setName(ORGCODE_RESTRICTION_CONFIG);
        orgCodeRestriction.setLabel("{{:: 'hiorgserver.orgcode-restriction' | translate}}");
        orgCodeRestriction.setHelpText("{{:: 'hiorgserver.orgcode-restriction.tooltip' | translate}}");

        return Arrays.asList(orgCodeRestriction);
    }

}
