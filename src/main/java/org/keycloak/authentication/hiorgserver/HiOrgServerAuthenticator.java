/**
 * Copyright 2020 Martin Böhmer
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keycloak.authentication.hiorgserver;

import java.util.ArrayList;
import java.util.List;
import org.jboss.logging.Logger;
import org.keycloak.authentication.AuthenticationFlowContext;
import org.keycloak.authentication.authenticators.broker.AbstractIdpAuthenticator;
import org.keycloak.authentication.authenticators.broker.util.SerializedBrokeredIdentityContext;
import org.keycloak.broker.hiorgserver.HiOrgServerIdentityContextWrapper;
import org.keycloak.broker.hiorgserver.HiOrgServerIdentityProviderConfig;
import org.keycloak.broker.hiorgserver.HiOrgServerIdentityProviderFactory;
import org.keycloak.broker.provider.BrokeredIdentityContext;
import org.keycloak.models.AuthenticatorConfigModel;
import org.keycloak.models.IdentityProviderModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;

/**
 * Restricts access to particular HiOrgServer organisations.
 *
 * @author Martin Böhmer
 */
public class HiOrgServerAuthenticator extends AbstractIdpAuthenticator {

    private static final Logger LOG = Logger.getLogger(HiOrgServerAuthenticator.class);

    @Override
    protected void authenticateImpl(AuthenticationFlowContext context, SerializedBrokeredIdentityContext serializedCtx, BrokeredIdentityContext brokerContext) {
        // Is restriction enabled?
        AuthenticatorConfigModel configModel = context.getAuthenticatorConfig();
        boolean orgCodeRestriction = true;
        if (configModel != null && configModel.getConfig() != null) {
            orgCodeRestriction = Boolean.valueOf(configModel.getConfig().get(HiOrgServerAuthenticatorFactory.ORGCODE_RESTRICTION_CONFIG));
            configModel.getAlias();
        }
        if (!orgCodeRestriction) {
            LOG.debugf("Access restriction based on HiOrg-Server organisation is disabled by configuration '%s'", configModel.getAlias());
            context.success();
            return;
        }
        // Has an organisation code been set for the IDP?
        String allowedOrgCode = getOrgCodeOfIdentityProvider(brokerContext.getIdpConfig());
        if (allowedOrgCode == null || allowedOrgCode.isEmpty()) {
            LOG.warnf("Access cannot be restricted to HiOrg-Server organisation as no Organisation Code has been set for identity provider '%s'",
                    brokerContext.getIdpConfig().getAlias());
            context.success();
            return;
        }
        // Enforce restriction
        LOG.debugf("Access is restricted to HiOrg-Server organisation '%s' configured for identity provider '%s'",
                allowedOrgCode, brokerContext.getIdpConfig().getAlias());
        // Determine brokered users org code
        HiOrgServerIdentityContextWrapper contextWrapper = new HiOrgServerIdentityContextWrapper(brokerContext);
        String usersOrgCode = contextWrapper.getOrganisationCode();
        // Do org codes match?
        if (allowedOrgCode.equalsIgnoreCase(usersOrgCode)) {
            LOG.debugf("Access granted: user is from expected HiOrg-Server organisation '%s'", usersOrgCode);
            context.success();
        } else {
            LOG.debugf("Access denied: user's HiOrg-Server organisation '%s' does not match the expected organisation '%s'", usersOrgCode, allowedOrgCode);
            context.cancelLogin();
        }
    }

    @Override
    protected void actionImpl(AuthenticationFlowContext context, SerializedBrokeredIdentityContext serializedCtx, BrokeredIdentityContext brokerContext) {
        // NOOP
    }

    private List<IdentityProviderModel> getHiOrgServerIdentityProviders(RealmModel realm) {
        List<IdentityProviderModel> hiorgIdpList = new ArrayList<>();
        String hiorgIdpId = HiOrgServerIdentityProviderFactory.PROVIDER_ID;
        for (IdentityProviderModel idp : realm.getIdentityProviders()) {
            if (hiorgIdpId.equals(idp.getProviderId())) {
                hiorgIdpList.add(idp);
            }
        }
        return hiorgIdpList;
    }

    private List<String> getAllConfiguredOrganisationCodes(RealmModel realm) {
        List<IdentityProviderModel> hiorgIdpList = getHiOrgServerIdentityProviders(realm);
        List<String> orgCodes = new ArrayList<>();
        for (IdentityProviderModel idp : hiorgIdpList) {
            HiOrgServerIdentityProviderConfig hiorgIdpConfig
                    = new HiOrgServerIdentityProviderConfig(idp);
            String orgCodeConfig = hiorgIdpConfig.getOrganisationCode();
            if (orgCodeConfig != null && !orgCodeConfig.isEmpty()) {
                orgCodes.add(orgCodeConfig);
            }
        }
        return orgCodes;
    }

    private String getOrgCodeOfIdentityProvider(IdentityProviderModel idp) {
        HiOrgServerIdentityProviderConfig idpConfig
                = new HiOrgServerIdentityProviderConfig(idp);
        return idpConfig.getOrganisationCode();
    }

    @Override
    public boolean requiresUser() {
        return false;
    }

    @Override
    public boolean configuredFor(KeycloakSession session, RealmModel realm, UserModel user) {
        List<IdentityProviderModel> hiorgsrvIdps = getHiOrgServerIdentityProviders(realm);
        boolean isConfiguredFor = !hiorgsrvIdps.isEmpty();
        if (LOG.isDebugEnabled()) {
            List<String> idpAliases = new ArrayList<>();
            for (IdentityProviderModel idp : hiorgsrvIdps) {
                idpAliases.add(idp.getAlias());
            }
            LOG.debugf("Is configured for: %s, found HiOrg-Server Identity Providers: %s", isConfiguredFor, idpAliases);
        }
        return isConfiguredFor;
    }

}
